public class ThreadSyncronization extends Thread {
    private String msg;
    private Thread t;
    private Sender sender;

    public ThreadSyncronization (Sender sender, String msg){
        this.sender = sender;
        this.msg = msg;
    }

    public void run(){
        synchronized (sender){
            sender.send (this.msg);
        }
    }
}

