public class SynchronizationTest {
    public static void main (String[] args){
        Sender s1 = new Sender();
        ThreadSyncronization t1 = new ThreadSyncronization ( s1, "Hi Terry Williams");
        ThreadSyncronization t2 = new ThreadSyncronization(s1, "We hope you enjoy the stay at our hotel!");

        t1.start();
        t2.start();

        try{
            t1.join();
            t2.join();
        }catch (InterruptedException x){
        }
    }

}
